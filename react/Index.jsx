/* eslint-disable no-console */
import React, { useState } from 'react';
import { injectIntl } from 'react-intl'
import { useCssHandles } from 'vtex.css-handles'
//import { MyComponentProps } from './typings/global'
import { useQuery } from 'react-apollo'
import QUERY_VALUE from './types/checkout.gql'
import QUERY_ORDER from './types/orderId.gql'
import QUERY_VARIANTS from './types/variants.gql'
import { useProduct } from 'vtex.product-context'
import { useLazyQuery } from 'react-apollo'
import $ from "jquery"
import { Fragment } from 'react';
import { render } from 'react-dom'
//import * as request from 'https'

//Declare Handles for the react component to be accesible
const CSS_HANDLES = [
  'someHandle1',
  'someHandle2',
  'someHandle3',

  'pack_propulsow',
  'pp_div',
  'pp_titulo',

  'pp_item_primero',
  'pp_elemento',
  'productIdPrimero',
  'productIdSuge',
  'pp_contenedor_sup',
  'pp_item_imagen',
  'pp_item_imagen_img',
  'pp_item_imagen_hidden',
  'pp_imagen',
  'pp_item_imagen',
  'pp_item_contenido_dos',
  'pp_item_contenido',
  'pp_item_nombre',
  'pp_item_precio',

  'pp_operador-01',
  'pp_operador',

  'pp_item_segundo',
  'pp_item_actions',
  'pp_contenedor_boton_cambiar',
  'pp_item_flechas',
  'pp_boton_cambiar',
  'pp_contenedor_boton_eliminar',
  'pp_boton_eliminar',
  'pp_item_x',
  'pp_boton_agregar',

  'pp_total',
  'pp_total_p',
  'pp_total_icono',
  'total__price',
  'pp_boton_comprar',

  'pp_item_tercero',

  'pp_select',
  'pp_contenedor_inferior',
  'pp_option_tallas',
  'pp_item_tallas_text',
  'pp_item_tallas',
  'pp_item_tallas_off',
]

const MyComponent = () => {
  const [loadOrder, datas] = useLazyQuery(QUERY_ORDER)


  const handles = useCssHandles(CSS_HANDLES)
  const productContextValue = useProduct()
  /*   const categoria = productContextValue?.product?.categoryId */
  console.log("prodcont", productContextValue.product)
  const [categoria, setCategoria] = useState("bait");
  let variants = {};
  if (productContextValue.product.categoryTree && categoria == "bait") {
    console.log("prodcont", productContextValue.product.categoryTree[0].id)
    setCategoria(productContextValue.product.categoryTree[0].id.toString());
  }

  const currency = function(number){
    return new Intl.NumberFormat('es-CL', {style: 'currency',currency: 'CLP', minimumFractionDigits: 0}).format(number);};

  console.log("cat", categoria);
  variants = useQuery(QUERY_VARIANTS, { variables: { categoria } });
  console.log("varia", variants)

  if (window.attachEvent) {
    window.attachEvent('onload', window.setTimeout(Suma, 600))
  } else {
    if (window.onload) {
      var curronload = window.onload
      var newonload = function (evt) {
        curronload(evt)
        window.setTimeout(Suma(evt), 600)
      }
      window.onload = newonload
    } else {
      window.onload = window.setTimeout(Suma, 600)
    }
  }

  function Suma() {
    if (variants.data == undefined) {
      chargeQuery()
    }
    let action = $(".kayserltda-complementa-tu-compra-1-x-pp_item_actions")
    let precios = $(".kayserltda-complementa-tu-compra-1-x-pp_item_precio")
    let pfirst = 0
    if (precios) {
      pfirst = parseInt(precios[0].attributes[1].value)
    }
    let pthird = 0
    let psecond = 0

    if (action[0].attributes[1].textContent == 1) {
      psecond = parseInt(precios[1].attributes[1].value)
    }
    if (action[1].attributes[1].textContent == 1) {
      pthird = parseInt(precios[2].attributes[1].value)
    }

    let total = pfirst + psecond + pthird
    const money = total;
    
    ReactDOM.render(
      <Fragment>{currency(money)}</Fragment>,
      document.getElementById('suma-final')
    )

    let vari = $('.kayserltda-complementa-tu-compra-1-x-pp_option_tallas')
    if (vari[0].childNodes.length == 0) {
      agregarVariables()
    }
    if (variants.data.products.length > 0) {
      cargarVariables2()
    }
  }
  function cargarVariables2() {
    let items = $('.kayserltda-complementa-tu-compra-1-x-productIdSuge')
    let item1 = items[0].attributes[3].value
    let item2 = items[1].attributes[3].value
    let prod1 = items[0].attributes[2].value
    let prod2 = items[1].attributes[2].value
    $('#pp_item_tallas_2').empty()
    $('#pp_item_tallas_3').empty()

    /*PRODUCTO 1 */
    if(variants.data.products.length>0){
    for (let i = 0; i < variants.data.products.length; i++) {
      if (variants.data.products[i].productId == prod1) {
        for (let f = 0; f < variants.data.products[i].items.length; f++) {
          if (variants.data.products[i].items[f].sellers[0].commertialOffer.AvailableQuantity > 0) {
            
            if(variants.data.products[i].items[f].variations.length>2 && variants.data.products[i].items[f].variations[2].name == "Copa")
            {
              document.getElementById('pp_item_tallas_2').insertAdjacentHTML('beforeend',
              "<option id=" + variants.data.products[i].items[f].itemId + ">" + variants.data.products[i].items[f].name +' / '+ variants.data.products[i].items[f].variations[2].values[0] + "</option>");
            }else{
              document
                .getElementById('pp_item_tallas_2')
                .insertAdjacentHTML(
                  'beforeend',
                  "<option id=" + variants.data.products[i].items[f].itemId + ">" + variants.data.products[i].items[f].name + "</option>");
            }
          }
        }
      }

      /*PRODUCTO 2 */
      if (variants.data.products[i].productId == prod2) {
        for (let f = 0; f < variants.data.products[i].items.length; f++) {
          if (variants.data.products[i].items[f].sellers[0].commertialOffer.AvailableQuantity > 0) {

            if(variants.data.products[i].items[f].variations.length>2 && variants.data.products[i].items[f].variations[2].name == "Copa")
            {
              document.getElementById('pp_item_tallas_3').insertAdjacentHTML('beforeend',
                  "<option id=" + variants.data.products[i].items[f].itemId + ">" + variants.data.products[i].items[f].name +' / '+ variants.data.products[i].items[f].variations[2].values[0] + "</option>");
            }else{
              document
                .getElementById('pp_item_tallas_3')
                .insertAdjacentHTML(
                  'beforeend',
                  "<option id=" + variants.data.products[i].items[f].itemId + ">" + variants.data.products[i].items[f].name + "</option>");
            }
          }
        }
      }
    }
  }
  }

  function HeadComp(props) {
    let agregarId = 'agregar-' + props.id
    let eliminarId = 'eliminar-' + props.id
    let cambiarId = 'cambiar-' + props.id
    let id = props.id

    if (props.estado == 1) {
      return (
        <div className={handles.pp_item_actions} name={props.estado}>
          <div className={handles.pp_contenedor_boton_cambiar}>
            <img
              className={`${handles.pp_item_flechas}`}
              src="/arquivos/flechas.svg"
              alt=""
            />
            <button
              id={cambiarId}
              style={{ display: '' }}
              className={handles.pp_boton_cambiar}
              type="button"
              onClick={e => Cambiar(id, e)}
            >
              Cambiar
            </button>
          </div>
          <div className={handles.pp_contenedor_boton_eliminar}>
            <img
              className={`${handles.pp_item_x}`}
              src="/arquivos/x.svg"
              alt=""
            />
            <button
              id={eliminarId}
              style={{ display: '' }}
              className={handles.pp_boton_eliminar}
              type="button"
              onClick={e => Eliminar(id, e)}
            >
              <i></i> Eliminar
            </button>
          </div>
          <button
            id={agregarId}
            style={{ display: 'none' }}
            className={handles.pp_boton_agregar}
            type="button"
            onClick={e => Agregar(id, e)}
          >
            <i className="fas fa-plus"></i>
            Agregar
          </button>
        </div>
      )
    } else {
      return (
        <div className={handles.pp_item_actions} name={props.estado}>
          <button
            id={cambiarId}
            style={{ display: 'none' }}
            className={handles.pp_boton_cambiar}
            type="button"
            onClick={e => Cambiar(id, e)}
          >
            <i className="fas fa-sync"></i>
            Cambiar
          </button>
          <button
            id={eliminarId}
            style={{ display: 'none' }}
            className={handles.pp_boton_eliminar}
            type="button"
            onClick={e => Eliminar(id, e)}
          >
            <i></i>
            Eliminar
          </button>
          <button
            id={agregarId}
            style={{ display: '' }}
            className={handles.pp_boton_agregar}
            type="button"
            onClick={e => Agregar(id, e)}
          >
            <i className="fas fa-plus"></i>
            Agregar
          </button>
        </div>
      )
    }
  }

  function Producto(props) {
    return (
      <Fragment>
        <input
          type="hidden"
          className={`${handles.productIdPrimero}`}
          value={props.ide}
        />
        <a className={`${handles.pp_item_imagen}`}>
          <img
            className={`${handles.pp_item_imagen_img}`}
            src={props.img}
            alt=""
          />
        </a>
        <div className={`${handles.pp_item_contenido}`}>
          <a className={`${handles.pp_item_nombre}`} href="#">
            <p className={`${handles.pp_item_nombre}`}>{props.name}</p>
          </a>

          <p className={`${handles.pp_item_precio}`} value={props.price}>{currency(props.price)}</p>
          <div className={`${handles.pp_contenedor_inferior}`}>
          <div className={`${handles.pp_item_tallas}`} id="1">
            <span className={`${handles.pp_item_tallas_text}`}>Tallas: </span>
            <select
              name="name-1"
              className="kayserltda-complementa-tu-compra-1-x-pp_option_tallas"
              id="pp_item_tallas_1"
            ></select>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }

  let producto = <Producto name={productContextValue?.product?.productName}
    img={productContextValue?.product?.items[0]?.images[0].imageUrl}
    ide={productContextValue?.product?.items[0]?.itemId}
    price={productContextValue?.product?.priceRange?.listPrice?.lowPrice}
  />

  function agregarVariables() {
    let prod = productContextValue.product
    /* console.log(prod) */
    let prods = $('.kayserltda-complementa-tu-compra-1-x-pp_item_contenido')

    for (let i = 0; i < prod.items.length; i++) {
      if (prod.items[i].sellers[0].commertialOffer.AvailableQuantity > 0) {
        if(prod.items[i].variations.length>2 && prod.items[i].variations[2].name == "Copa")
        {
          document
          .getElementById('pp_item_tallas_1')
          .insertAdjacentHTML(
            'beforeend',
            '<option id=' +
            prod.items[i].itemId +
            '>' +
            prod.items[i].name +' / '+ prod.items[i].variations[2].values[0] +
            '</option>'
          )
        }else{
          document
          .getElementById('pp_item_tallas_1')
          .insertAdjacentHTML(
            'beforeend',
            '<option id=' +
            prod.items[i].itemId +
            '>' +
            prod.items[i].name +
            '</option>'
          )
        }
      }
    }
  }

  function Cambiar(id, e) {
    let idselect = 'pp_item_tallas_' + id
    let neim = 'name-' + id
    e.preventDefault()
    let prod = Math.floor(Math.random() * variants.data.products.length)

    $('#elemento-' + id).html('')
    $('#elemento-' + id).append(
      '<input type="hidden" class="kayserltda-complementa-tu-compra-1-x-productIdSuge" value="' +variants.data.products[prod].productId +
        '" name="' + variants.data.products[prod].items[0].itemId +'" /><a class="kayserltda-complementa-tu-compra-1-x-pp_item_imagen" href="'+variants.data.products[prod].link +'"><img class="kayserltda-complementa-tu-compra-1-x-pp_item_imagen_img"src="' +
        variants.data.products[prod].items[0].images[0].imageUrl +
        '" /></a><div class="kayserltda-complementa-tu-compra-1-x-pp_item_contenido"  id="' +
        variants.data.products[prod].items[0].itemId +
        '"><a class="kayserltda-complementa-tu-compra-1-x-pp_item_nombre" href="'+variants.data.products[prod].link +'"><p class="kayserltda-complementa-tu-compra-1-x-pp_item_nombre">' +
        variants.data.products[prod].productName +
        '</p></a><p class="kayserltda-complementa-tu-compra-1-x-pp_item_precio" value="'+variants.data.products[prod].items[0].sellers[0].commertialOffer.ListPrice+'">' +
        currency(variants.data.products[prod].items[0].sellers[0].commertialOffer.ListPrice) +
        '</p></div><div class="kayserltda-complementa-tu-compra-1-x-pp_contenedor_inferior"><div class="kayserltda-complementa-tu-compra-1-x-pp_item_tallas" id=' +
        id +
        '><span class="kayserltda-complementa-tu-compra-1-x-pp_item_tallas_text">Tallas: </span><select name=' +
        neim +
        ' class="kayserltda-complementa-tu-compra-1-x-pp_option_tallas" id=' +
        idselect +
        '></select></div></div>'
    )
    window.setTimeout(Suma, 400)
  }

  function Eliminar(id, e) {
    e.preventDefault()

    ReactDOM.render(
      <HeadComp id={id} estado="2" />,
      document.getElementById('head-' + id)
    )
    window.setTimeout(Suma, 400)
  }

  function Agregar(id, e) {
    e.preventDefault()

    ReactDOM.render(
      <HeadComp id={id} estado="1" />,
      document.getElementById('head-' + id)
    )
    window.setTimeout(Suma, 400)
  }

  function chargeQuery() {
    loadOrder()
    let orderForm = datas.data.orderForm.id
    let aprob = datas.called
    let other = datas

    return [orderForm, aprob, other]
  }
  let charge = ''

  function FinalizarCompra(e) {
    e.preventDefault()

    charge = chargeQuery()
    console.log(charge)
    let dada = charge[0]

    let skuFirst = $('select[name="name-1"] option:selected').attr('id')
    let skuSecond = $('select[name="name-2"] option:selected').attr('id')
    let skuThird = $('select[name="name-3"] option:selected').attr('id')
    let actives = $(".kayserltda-complementa-tu-compra-1-x-pp_item_actions")
    let active2 = actives[0].attributes[1].value
    let active3 = actives[1].attributes[1].value
    console.log("actives", active2, active3)

    console.log('variante1', skuFirst)
    console.log('variante2', skuSecond)
    console.log('variante3', skuThird)

    let cuerpo = '{"orderItems":['
    cuerpo = cuerpo + '{"quantity":1,"seller":"1","id":"' + skuFirst + '"},'
    if (active2 == 1) {
      cuerpo = cuerpo + '{"quantity":1,"seller":"1","id":"' + skuSecond + '"},'
    }
    if (active3 == 1) {
      cuerpo = cuerpo + '{"quantity":1,"seller":"1","id":"' + skuThird + '"},'
    }
    cuerpo = cuerpo + ']}'
    console.log("cuerpo", cuerpo)
    fetch('/api/checkout/pub/orderForm/' + dada + '/items', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: cuerpo,
    })
      .then(r => r.json())
      .then(data => console.log('data returned:', data))

    window.location.href = 'https://www.kaysershop.com//checkout/#/email'
  }
  if(productContextValue?.product?.priceRange?.listPrice?.lowPrice!=null)
{
  let r1;
  let r2;
  if(variants.data!=undefined && variants.data.products.length>0){
  r1=Math.trunc(Math.random()*variants.data.products.length);
  r2=Math.trunc(Math.random()*variants.data.products.length);
  console.log("randoms",r1,r2)
  }
  return (
    <div>
      {variants.data != undefined && variants.data.products.length > 0 && r1!=r2 && (
        <div id="pack_propulsow" className={`${handles.pack_propulsow}`}>
          <div id="pp_div" className={`${handles.pp_div}`}>
            <div className={`${handles.pp_titulo}`}>Complementa tu compra</div>

            {/*Primer elemento*/}
            <div className={`${handles.pp_elemento}`}>
              <div className={`${handles.pp_item_primero}`}>
                {producto}
                
              </div>

              {/*Operador suma*/}
              <div className={`${handles.pp_operador}`}>+</div>

              {/*Segundo elemento*/}
              <div className={`${handles.pp_item_segundo}`}>
                <div id="elemento-2">
                  <input type="hidden" className="kayserltda-complementa-tu-compra-1-x-productIdSuge" value={variants.data.products[r1].items[0].itemId} name={variants.data.products[r1].productId} />
                  <a className={`${handles.pp_item_imagen}`} href={variants.data.products[r1].link}>
                    <img className={`${handles.pp_item_imagen_img}`} src={variants.data.products[r1].items[0].images[0].imageUrl} />
                  </a>
                  <div className="kayserltda-complementa-tu-compra-1-x-pp_item_contenido" id={variants.data.products[r1].items[0].itemId}>
                    <a className="kayserltda-complementa-tu-compra-1-x-pp_item_nombre" href={variants.data.products[r1].link}>
                      <p className="kayserltda-complementa-tu-compra-1-x-pp_item_nombre">{variants.data.products[r1].productName}</p></a>
                      <p className="kayserltda-complementa-tu-compra-1-x-pp_item_precio" value={variants.data.products[r1].items[0].sellers[0].commertialOffer.ListPrice}> {currency(variants.data.products[r1].items[0].sellers[0].commertialOffer.ListPrice)}</p>
                    
                  </div>


                  <div className={`${handles.pp_contenedor_inferior}`}>
                    <div className={`${handles.pp_item_tallas}`} id="2">
                      <span className={`${handles.pp_item_tallas_text}`}>
                        Tallas:{' '}
                      </span>
                      <select
                        name="name-2"
                        className={`${handles.pp_option_tallas}`}
                        id="pp_item_tallas_2"
                      ></select>
                    </div>
                  </div>
                  </div>
                <div id="head-2">
                  <HeadComp id="2" estado="1" />
                </div>
                
              </div>

              {/*Operador*/}
              <div className={`${handles.pp_operador}`}>+</div>

              {/*Tercer elemento*/}
              <div className={`${handles.pp_item_tercero}`}>
                <div id="elemento-3">
                  <input type="hidden" class="kayserltda-complementa-tu-compra-1-x-productIdSuge" value={variants.data.products[r2].items[0].itemId} name={variants.data.products[r2].productId} />
                  <a class="kayserltda-complementa-tu-compra-1-x-pp_item_imagen" href={variants.data.products[r2].link}>
                    <img className={`${handles.pp_item_imagen_img}`} src={variants.data.products[r2].items[0].images[0].imageUrl} />
                  </a><div class="kayserltda-complementa-tu-compra-1-x-pp_item_contenido" id={variants.data.products[r2].items[0].itemId}>
                    <a class="kayserltda-complementa-tu-compra-1-x-pp_item_nombre" href={variants.data.products[r2].link}>
                      <p class="kayserltda-complementa-tu-compra-1-x-pp_item_nombre">{variants.data.products[r2].productName}</p></a>
                      <p class="kayserltda-complementa-tu-compra-1-x-pp_item_precio" value={variants.data.products[r2].items[0].sellers[0].commertialOffer.ListPrice}> {currency(variants.data.products[r2].items[0].sellers[0].commertialOffer.ListPrice)}</p>
                    
                  </div>


                  <div className={`${handles.pp_contenedor_inferior}`}>
                    <div className={`${handles.pp_item_tallas}`} id="2">
                      <span className={`${handles.pp_item_tallas_text}`}>
                        Tallas:{' '}
                      </span>
                      <select
                        name="name-3"
                        className={`${handles.pp_option_tallas}`}
                        id="pp_item_tallas_3"
                      ></select>
                    </div>
                    </div>
                    </div>
                <div id="head-3">
                  <HeadComp id="3" estado="1" />
                </div>
                
                

              </div>


              {/*Operador Igual*/}
              <div className={`${handles.pp_operador}`}>=</div>

              {/*Total*/}
              <div className={`${handles.pp_total}`}>
                <div className={`${handles.pp_total_icono}`}></div>
                <p className={`${handles.pp_total_p}`}>
                  Comprar 3 productos por
                </p>
                <span id="suma-final" className={`${handles.total__price}`}>
                  <div id="div-suma"></div>
                </span>
                <button
                  onClick={e => FinalizarCompra(e)}
                  className={`${handles.pp_boton_comprar}`}
                  type="button"
                >
                  Comprar
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  )
      } 
}

export default injectIntl(MyComponent)
